
export const deleteUser = async id => {
  return await fetch(`http://localhost:8080/users/${id}`, {
    method: 'DELETE'
  });
}

export const loadUsers = async () => {
  return await fetch('http://localhost:8080/users')
    .then(resp => resp.json());
};

export const loadUser = async id => {
  return await fetch(`http://localhost:8080/users/${id}`)
    .then(resp => resp.json());
};

export const addUser = async user => {
  return await fetch('http://localhost:8080/users', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(user)
  })
    .then(resp => resp.json());
};

export const authUser = async user => {
  return await fetch('http://localhost:8080/api/auth/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(user)
  })
    .then(resp => {
      if (resp.status !== 200) {
        throw new Error('Wrong username or password!');
      }
      return resp.json();
    });
};
