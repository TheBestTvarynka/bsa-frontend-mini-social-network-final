
export const loadMessages = async () => {
  return await fetch('http://localhost:8080/messages')
    .then(resp => resp.json());
};

export const sendMessage = async message => {
  return await fetch(`http://localhost:8080/messages`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(message)
  })
    .then(resp => resp.json());
}

export const deleteMessage = async id => {
  return await fetch(`http://localhost:8080/messages/${id}`, {
    method: 'DELETE'
  });
};

export const loadMessage = async id => {
  return await fetch(`http://localhost:8080/messages/${id}`)
    .then(resp => resp.json());
};

export const loadMedia = async () => {
  return await fetch('https://thebesttravynka.github.io/chat-data/media.json')
    .then(resp => resp.json());
};

export const loadFiles = async () => {
  return await fetch('https://thebesttravynka.github.io/chat-data/files.json')
    .then(resp => resp.json());
};
