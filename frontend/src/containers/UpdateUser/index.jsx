import React, {useEffect, useState} from 'react';
import { connect } from 'react-redux';
import {Link, Redirect, useParams} from 'react-router-dom';
import { loadUserAction, setUserAction, addUserAction } from '../Messenger/actions';
import Spinner from '../../components/Spinner';

import './UpdateUser.css';
import '../../components/LoginPage/LoginPage.css';

const UpdateUser = ({ profile, user, loadUserAction, setUserAction, addUserAction }) => {
  const { id } = useParams();
  if (!user && !id) {
    setUserAction({ url: '', username: '' });
  }
  if (!user && id) {
    loadUserAction(id);
  }

  const [url, setUrl] = useState(user ? user.url  : '');
  const [username, setUsername] = useState(user ? user.user  : '');

  useEffect(() => {
    if (user) {
      setUrl(user.url);
      setUsername(user.user);
    }
  }, [user]);

  const handleCancel = () => {
    setUserAction(undefined);
  };

  const handleUpdate = () => {
    addUserAction({ id, url, user: username });
    setUserAction(undefined);
  };

  return (
    <div className="update_user_back">
      {profile
      ? (profile.role === 'ADMIN'
        ? (user
            ? (<form className="login_form">
              <img src={url} alt="" className="user_avatar_preview"/>
              <label className="input_title">User id:</label>
              {id && <label className="user_id">{id}</label>}
              <label className="input_title">Avatar url:</label>
              <input className="login_input"
                     placeholder="e. g. https://site.com/image.jpg"
                     value={url}
                     onChange={ev => setUrl(ev.target.value)}/>
              <label className="input_title">Username:</label>
              <input className="login_input"
                     placeholder="e. g. capmap"
                     value={username}
                     onChange={ev => setUsername(ev.target.value)}/>
              <div className="update_actions">
                <Link to="/users">
                  <button className="cancel" onClick={handleCancel}>Cancel</button>
                </Link>
                <Link to="/users">
                  <button className="update" onClick={handleUpdate}>Continue</button>
                </Link>
              </div>
            </form>)
            : <Spinner />)
        : <Redirect to={{ pathname: '/error', state: { from: '/users' } }} />)
      : <Redirect to={{ pathname: '/login', state: { from: `/user/${id}` } }} />}
    </div>
  );
};

const mapStateToProps = rootState => ({
  profile: rootState.profile,
  user: rootState.user
});

const mapDispatchToProps = {
  loadUserAction,
  setUserAction,
  addUserAction
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdateUser);