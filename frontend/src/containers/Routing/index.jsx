import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import UpdateMessage from '../UpdateMessage';
import LoginPage from '../../components/LoginPage';
import UsersList from '../UsersList';
import UpdateUser from '../UpdateUser';
import ErrorPage from '../../components/ErrorPage';
import Chat from '../Chat';

const Routing = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Chat />
        </Route>
        <Route exact path="/login">
          <LoginPage />
        </Route>
        <Route exact path="/message/:id">
          <UpdateMessage />
        </Route>
        <Route exact path="/users">
          <UsersList />
        </Route>
        <Route path="/user/:id">
          <UpdateUser />
        </Route>
        <Route path="/user">
          <UpdateUser />
        </Route>
        <Route exact path="/error">
          <ErrorPage />
        </Route>
        <Route path="*" exact>
          <ErrorPage />
        </Route>
      </Switch>
    </Router>
  );
};

export default Routing;