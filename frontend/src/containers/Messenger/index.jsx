import React from 'react';
import { connect } from 'react-redux';
import { sendMessageAction, deleteMessageAction, reactMessageAction, loadMessagesAction } from './actions';
import Spinner from '../../components/Spinner';
import MessageList from '../MessageList';
import MessageInput from '../../components/MessageInput';
import { Redirect } from 'react-router-dom';

import './Messenger.css';

const Messenger = ({ messages, profile, sendMessageAction, deleteMessageAction, toggleExpandedMessage, reactMessageAction, loadMessagesAction, loadUserAction }) => {
  if (!messages) loadMessagesAction();

  return (
    <div className="messenger">
      {(messages === undefined)
      ? <Spinner />
      : (profile
      ? <MessageList messages={messages} user={profile.user} deleteMessage={deleteMessageAction} toggleExpandedMessage={toggleExpandedMessage} reactMessage={reactMessageAction}/>
      : <Redirect to={{ pathname: '/login', state: { from: '/' } }} />)}
      <MessageInput sendMessage={sendMessageAction} />
    </div>
  );
}

const mapStateToProps = rootState => ({
  messages: rootState.messages,
  profile: rootState.profile
});

const mapDispatchToProps = {
  sendMessageAction,
  deleteMessageAction,
  reactMessageAction,
  loadMessagesAction
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Messenger);