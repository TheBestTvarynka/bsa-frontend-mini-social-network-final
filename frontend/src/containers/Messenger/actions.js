import {
  DELETE_MESSAGE,
  SEND_MESSAGE,
  SET_ALL_MESSAGES,
  SET_EXPANDED_MESSAGE,
  SET_EXPANDED_MEDIA,
  SET_USER,
  ADD_USER,
  AUTH_USER,
  SET_AUTH_USER,
  DELETE_USER,
  SET_ALL_USERS,
  SET_ALL_MEDIA,
  SET_ALL_FILES,
  LOAD_MESSAGES,
  LOAD_MESSAGE,
  LOAD_USERS,
  LOAD_USER,
  LOAD_MEDIA,
  LOAD_FILES,
  REACT_MESSAGE
} from './actionTypes';

export const loadMessagesAction = () => ({
  type: LOAD_MESSAGES
});

export const loadMessageAction = id => ({
  type: LOAD_MESSAGE,
  id
});

export const loadUsersAction = () => ({
  type: LOAD_USERS
});

export const loadUserAction = id => ({
  type: LOAD_USER,
  id
});

export const addUserAction = user => ({
  type: ADD_USER,
  user
})

export const loadMediaAction = () => ({
  type: LOAD_MEDIA
});

export const loadFilesAction = () => ({
  type: LOAD_FILES
});

export const sendMessageAction = message => ({
  type: SEND_MESSAGE,
  message
});

export const deleteMessageAction = id => ({
  type: DELETE_MESSAGE,
  id
});

export const reactMessageAction = id => ({
  type: REACT_MESSAGE,
  id
});

export const setALLMessagesAction = messages => ({
  type: SET_ALL_MESSAGES,
  messages
});

export const setUserAction = user => ({
  type: SET_USER,
  user
});

export const deleteUserAction = id => ({
  type: DELETE_USER,
  id
});

export const authUserAction = user => ({
  type: AUTH_USER,
  user
});

export const setAuthUserAction = profile => ({
  type: SET_AUTH_USER,
  profile
})

export const setALLUsersAction = users => ({
  type: SET_ALL_USERS,
  users
});

export const setALLMediaAction = media => ({
  type: SET_ALL_MEDIA,
  media
});

export const setALLFilesAction = files => ({
  type: SET_ALL_FILES,
  files
});

export const setExpandedMessageAction = expandedMessage => ({
  type: SET_EXPANDED_MESSAGE,
  expandedMessage
})

export const setExpandedMediaAction = expandedMedia => ({
  type: SET_EXPANDED_MEDIA,
  expandedMedia
})

export const reactMessage = messageId => async (dispatch, getRootState) => {
  const { messages } = getRootState();
  const updatedMessages = messages.map(message => message.id === messageId
  ? { ...message, isLiked: !message.isLiked }
  : message);
  dispatch(setALLMessagesAction(updatedMessages));
};

export const formatDate = stringDate => {
  return new Date(stringDate.toString()).toLocaleTimeString();
};
