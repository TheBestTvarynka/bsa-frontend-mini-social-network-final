import { put, takeEvery, all } from 'redux-saga/effects';
import {
  setALLMessagesAction,
  setUserAction,
  setALLUsersAction,
  setALLMediaAction,
  setALLFilesAction,
  setExpandedMessageAction,
  setAuthUserAction
} from './actions';
import * as chatService from '../../services/chatService';
import * as userService from '../../services/userService';
import {
  LOAD_MESSAGES,
  LOAD_USERS,
  LOAD_MEDIA,
  LOAD_FILES,
  LOAD_USER,
  ADD_USER,
  DELETE_USER,
  DELETE_MESSAGE,
  SEND_MESSAGE,
  LOAD_MESSAGE,
  AUTH_USER,
  REACT_MESSAGE
} from './actionTypes';

function* fetchMessages() {
  try {
    const messages = yield chatService.loadMessages();
    yield put(setALLMessagesAction(messages));
  } catch (err) {
    console.log(err);
  }
}
function* loadMessages() {
  yield takeEvery(LOAD_MESSAGES, fetchMessages);
}

function* fetchUsers() {
  try {
    const users = yield userService.loadUsers();
    yield put(setALLUsersAction(users));
  } catch (err) {
    console.log(err);
  }
}
function* loadUsers() {
  yield takeEvery(LOAD_USERS, fetchUsers);
}

function* fetchUser(action) {
  try {
    const user = yield userService.loadUser(action.id);
    yield put(setUserAction(user));
  } catch (err) {
    console.log(err);
  }
}
function* loadUser() {
  yield takeEvery(LOAD_USER, fetchUser);
}

function* fetchMedia() {
  try {
    const media = yield chatService.loadMedia();
    yield put(setALLMediaAction(media));
  } catch (err) {
    console.log(err);
  }
}
function* loadMedia() {
  yield takeEvery(LOAD_MEDIA, fetchMedia);
}

function* fetchFiles() {
  try {
    const files = yield chatService.loadFiles();
    yield put(setALLFilesAction(files));
  } catch (err) {
    console.log(err);
  }
}
function* loadFiles() {
  yield takeEvery(LOAD_FILES, fetchFiles);
}

function* addMessage(action) {
  const message = action.message;
  try {
    const messages = yield chatService.loadMessages();
    if (!message.id) {
      // add new message
      message.createdAt = new Date().toISOString();
      message.editedAt = '';
      const newMessage = yield chatService.sendMessage(message);
      yield put(setALLMessagesAction([...messages, newMessage]));
    } else {
      // update message
	  message.editedAt = new Date().toISOString();
      const newMessage = yield chatService.sendMessage(message);
      const updated = messages.map(m => m.id === newMessage.id
        ? newMessage
        : m);
      yield put(setALLMessagesAction(updated));
    }
  } catch (err) {
    console.log(err);
  }
}
function* addMessageWatcher() {
  yield takeEvery(SEND_MESSAGE, addMessage);
}

function* addUser(action) {
  const user = action.user;
  yield userService.addUser(user);
  const users = yield userService.loadUsers();
  yield put(setALLUsersAction(users));
}
function* addUserWatcher() {
  yield takeEvery(ADD_USER, addUser);
}

function* authUser(action) {
  try {
    const authUser = action.user;
    const profile = yield userService.authUser(authUser);
    yield put(setAuthUserAction(profile));
  } catch (err) {
    console.log(err);
    alert(err);
  }
}
function* authUserWatcher() {
  yield takeEvery(AUTH_USER, authUser);
}

function* deleteMessage(action) {
  try {
    const messageId = action.id;
    yield chatService.deleteMessage(messageId);
    const messages = yield chatService.loadMessages();
    yield put(setALLMessagesAction(messages));
  } catch (err) {
    console.log(err);
  }
}
function* deleteMessageWatcher() {
  yield takeEvery(DELETE_MESSAGE, deleteMessage);
}

function* reactMessage(action) {
  try {
    const id = action.id;
    const message = yield chatService.loadMessage(id);
    const messages = yield chatService.loadMessages();

    const updatedMessage = { ...message, isLiked: !message.isLiked };
    const newMessage = yield chatService.sendMessage(updatedMessage);
    const updated = messages.map(m => m.id === newMessage.id
      ? updatedMessage
      : m);
    yield put(setALLMessagesAction(updated));
  } catch (err) {
    console.log(err);
  }
}
function* reactMessageWatcher() {
  yield takeEvery(REACT_MESSAGE, reactMessage);
}

function* deleteUser(action) {
  try {
    yield userService.deleteUser(action.id);
    const users = yield userService.loadUsers();
    yield put(setALLUsersAction(users));
  } catch (err) {
    console.log(err);
  }
}
function* deleteUserWatcher() {
  yield takeEvery(DELETE_USER, deleteUser);
}

function* fetchMessage(action) {
  try {
    const id = action.id;
    const message = yield chatService.loadMessage(id);
    yield put(setExpandedMessageAction(message));
  } catch (err) {
    console.log(err);
  }
}
function* loadMessage() {
  yield takeEvery(LOAD_MESSAGE, fetchMessage)
}

export default function* rootSaga() {
  yield all([
    loadMessages(),
    loadUsers(),
    loadUser(),
    loadMedia(),
    loadFiles(),
    addMessageWatcher(),
    addUserWatcher(),
    deleteMessageWatcher(),
    deleteUserWatcher(),
    authUserWatcher(),
    reactMessageWatcher(),
    loadMessage()
  ]);
}
