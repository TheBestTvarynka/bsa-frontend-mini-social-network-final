import {
  SET_ALL_MESSAGES,
  SET_EXPANDED_MESSAGE,
  SET_EXPANDED_MEDIA,
  SET_USER,
  SET_ALL_USERS,
  SET_ALL_MEDIA,
  SET_ALL_FILES,
  SET_AUTH_USER
} from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_AUTH_USER:
      return {
        ...state,
        profile: action.profile
      }
    case SET_ALL_MESSAGES:
      return {
        ...state,
        messages: action.messages
      };
    case SET_EXPANDED_MESSAGE:
      return {
        ...state,
        expandedMessage: action.expandedMessage
      };
    case SET_EXPANDED_MEDIA:
      return {
        ...state,
        expandedMedia: action.expandedMedia
      };
    case SET_USER:
      return {
        ...state,
        user: action.user
      }
    case SET_ALL_USERS:
      return {
        ...state,
        users: action.users
      }
    case SET_ALL_MEDIA:
      return {
        ...state,
        media: action.media
      }
    case SET_ALL_FILES:
      return {
        ...state,
        files: action.files
      }
    default:
      return state;
  }
};
