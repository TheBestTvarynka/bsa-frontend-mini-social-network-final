import React, { useState, useEffect } from 'react';
import {useParams, Link, Redirect} from 'react-router-dom';
import { connect } from 'react-redux';
import { sendMessageAction, loadMessageAction, setExpandedMessageAction } from '../Messenger/actions';
import Spinner from '../../components/Spinner';

import './UpdateMessage.css';

const UpdateMessage = ({ profile, message, loadMessageAction, sendMessageAction, setExpandedMessageAction }) => {
  // cut id from url
  const { id } = useParams();

  if (!message) loadMessageAction(id);

  const [text, setText] = useState(message ? message.text : '');
  const [userId, setUserId] = useState(message ? message.userId : '');
  useEffect(() => {
    if (message) {
      setText(message.text);
      setUserId(message.userId);
    }
  }, [message]);

  const handleUpdate = () => {
    sendMessageAction({ ...message, text});
    setExpandedMessageAction(undefined);
    setText('');
  };

  return (
    <div className="back">
      {profile
      ? ((message && userId)
        ? (profile.user.id === userId
          ? (<div className="update_message" id="modal_window">
              <span className="update_title">Edit message</span>
              <textarea value={text} onChange={ev => setText(ev.target.value)} />
              <div className="update_actions">
                <Link to="/" style={{ textDecoration: 'none'}}>
                  <span className="cancel">Cancel</span>
                </Link>
                <Link to="/" style={{ textDecoration: 'none'}} onClick={handleUpdate}>
                  <span className="update">Update</span>
                </Link>
              </div>
            </div>)
            : <span>Redirect</span>)
        :<Spinner />)
      : <Redirect to={{ pathname: '/login', state: { from: `/message/${id}` } }} />}
    </div>
  );
};

const mapStateToProps = rootState => ({
  message: rootState.expandedMessage,
  profile: rootState.profile
});

const mapDispatchToProps = {
  sendMessageAction,
  loadMessageAction,
  setExpandedMessageAction
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdateMessage);