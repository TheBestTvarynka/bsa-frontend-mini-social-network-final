import React from 'react';
import { connect } from 'react-redux';
import { loadUsersAction, loadMediaAction, loadFilesAction, setExpandedMediaAction } from '../Messenger/actions';
import { Link } from 'react-router-dom';
import Spinner from '../../components/Spinner/index';

import './ChatDetails.css'

const Index = ({ users, media, files, messages, loadUsersAction, loadMediaAction, loadFilesAction, toggleExpandedMedia }) => {
  const firstUsers = users ? users.slice(0, 6) : [];
  const firstMedia = media ? media.slice(0, 6) : [];
  const firstFiles = files ? files.slice(0, 3) : [];
  const lastMessage = messages ? new Date(messages[messages.length - 1].createdAt).toLocaleString() : [];

  if (!users) loadUsersAction();
  if (! media) loadMediaAction();
  if (!files) loadFilesAction();

  const renderMedia = (media, allMedia) => {
    const res = [];
    for (let i = 0; i < media.length; i++) {
      res.push(<img src={media[i].url} alt="" key={media[i].id} onClick={() => toggleExpandedMedia({ index: i, media: allMedia })}/>);
    }
    return res;
  };

  return (
    <div className="chat_details">
      <div className="chat_info">
        <img className="chat_logo" src="https://i.imgur.com/ZhyKAqX.jpeg" alt=""/>
        <span className="chat_name">BSA UI/UX</span>
        <span className="chat_last_message">last message at {lastMessage}</span>
      </div>
      <span className="chat_description">Discussion of Binary Studio Academy</span>
      <span className="title">Group Members</span>
      {users
        ? <div className="members">
            {firstUsers.map(user => (
              <img src={user.url} alt="" key={user.id}/>
            ))}
          </div>
        : <Spinner />}
      <Link to="/users" style={{ textDecoration: 'none'}}>
        <button className="view_all">View all ({users ? users.length : 0})</button>
      </Link>
      <span className="title">Photos & Multimedia</span>
      {media
        ? <div className="photos">
            {renderMedia(firstMedia, media)}
          </div>
        : <Spinner />}
      <button className="view_all" onClick={() => toggleExpandedMedia({ index: 0, media })}>View all ({media ? media.length : 0})</button>
      <span className="title">Attachments</span>
      <div className="attachments">
        <div className="file"><img src="https://img.icons8.com/fluent/48/000000/pdf.png" alt=""/><span>task-1.pdf</span></div>
        <div className="file"><img src="https://img.icons8.com/officel/30/000000/burn-cd.png" alt=""/><span>arch_linux.iso</span></div>
        <div className="file"><img src="https://img.icons8.com/dusk/64/000000/ms-powerpoint.png" alt=""/><span>why_bsa.pptx</span></div>
      </div>
      <button className="view_all">View all ({files ? files.length : 0})</button>
    </div>
  );
}

const mapStateToProps = rootState => ({
  users: rootState.users,
  media: rootState.media,
  files: rootState.files,
  messages: rootState.messages
});

const mapDispatchToProps = {
  loadUsersAction,
  loadMediaAction,
  loadFilesAction,
  toggleExpandedMedia: setExpandedMediaAction
};

// const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Index);