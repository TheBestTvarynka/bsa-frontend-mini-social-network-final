import React from 'react';
import { connect } from 'react-redux';
import { loadUsersAction } from '../Messenger/actions';
import Spinner from '../../components/Spinner';
import {Link, Redirect} from 'react-router-dom';
import UserListItem from '../../components/UserListItem';

import './UsersList.css';

const UsersList = ({ profile, users, loadUsersAction }) => {
  if (!users) loadUsersAction();

  return (
    <div className="users_list_back">
      {profile === undefined
        ? <Redirect to={{ pathname: '/login', state: { from: '/users' } }} />
        : (profile.role === 'ADMIN'
          ? (<div className="users_list">
              <span className="users_title">Users</span>
              <Link to="/user" style={{ textDecoration: 'none' }}>
                <button className="add_user">	&#43; add new user</button>
              </Link>
              <Link to="/" style={{ textDecoration: 'none' }}>
                <button className="back_to_chat">back to chat</button>
              </Link>
              {users
                ? users.map(user => (
                  <UserListItem user={user} key={user.id}/>
                ))
                : <Spinner />}
              </div>)
            : <Redirect to={{ pathname: '/error', state: { from: '/users' } }} />)}
    </div>
  );
}

const mapStateToProps = rootState => ({
  profile: rootState.profile,
  users: rootState.users
});

const mapDispatchToProps = {
  loadUsersAction
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersList);