import React, { useRef, useEffect, useState } from 'react';
import Message from '../../components/Message';
import DaySeparator from '../../components/DaySeparator';

import './MessageList.css';

const MessageList = ({ messages, user, deleteMessage, toggleExpandedMessage, reactMessage }) => {
  const messagesEndRef = useRef(null);

  const [messagesCount, setCount] = useState(-1);

  const scrollToBottom = () => {
    if (messages && messages.length === messagesCount) {
      return;
    }
    if (messages) {
      setCount(messages.length);
    }
    if (messagesEndRef.current) {
      messagesEndRef.current.scrollIntoView();
    }
  }
  useEffect(scrollToBottom, [messages]);

  // insert day separators between messages
  const messagesWithSeparators = [];
  for (let i = 0; i < messages.length - 1; i++) {
    messagesWithSeparators.push(messages[i]);
    const firstDate = new Date(messages[i].createdAt);
    const secondDate = new Date(messages[i + 1].createdAt);
    if (firstDate.getDate() !== secondDate.getDate()) {
      messagesWithSeparators.push({ id: i + '', date: secondDate.toDateString() })
    }
    if (messages[i].userId === messages[i + 1].userId) {
      messages[i].avatar = undefined;
    }
  }
  messagesWithSeparators.push(messages[messages.length - 1]);

  return (
    <div className="wrapper" id="message_list_wrapper">
      <div className="message_list">
        {messagesWithSeparators.map(message => (message.date
            ? <DaySeparator key={message.id} date={message.date} />
            : <Message key={message.id} data={message} user={user} deleteMessage={deleteMessage} reactMessage={reactMessage}/>
        ))}
      </div>
      <div ref={messagesEndRef} />
    </div>
  );
}

export default MessageList;