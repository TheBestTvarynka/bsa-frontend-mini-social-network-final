import React from 'react';
import { connect } from 'react-redux';
import Messenger from '../Messenger';
import ChatDetails from '../ChatDetails';
import Header from '../Header';
import MediaSlider from '../../components/MediaSlider';
import { Redirect } from 'react-router-dom';

import './Chat.css';

const Chat = ({ profile, expandedMedia }) => {
  return (
    <div className="chat">
      {profile === undefined &&
        <Redirect to={{ pathname: '/login', state: { from: '/' } }} />}
      <Header />
      <Messenger />
      <ChatDetails />
      {expandedMedia && <MediaSlider />}
    </div>
  );
}

const mapStateToProps = rootState => ({
  profile: rootState.profile,
  expandedMedia: rootState.expandedMedia
});

export default connect(
  mapStateToProps,
  null
)(Chat);