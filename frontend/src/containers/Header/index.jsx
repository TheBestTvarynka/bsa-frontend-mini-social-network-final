import React  from 'react';
import { connect } from 'react-redux';
import { setAuthUserAction } from '../Messenger/actions';

import './Header.css';

const Header = ({ profile, setAuthUserAction }) => {
  const user = profile ? profile.user : undefined;

  const handleLogOut = () => {
    setAuthUserAction(undefined);
  };

  return (
    <div className="header">
      <div className="app_name">
        <img src="https://img.icons8.com/cotton/64/000000/chat.png" alt=""/>
        <span>AmazingChatApp</span>
      </div>
      <div className="user_panel">
        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
             width="24" height="24"
             viewBox="0 0 172 172"
             style={{"fill": "#000000"}}>
          <g fill="none" fillRule="nonzero" stroke="none" strokeWidth="1" strokeLinecap="butt" strokeLinejoin="miter" strokeMiterlimit="10" strokeDasharray="" strokeDashoffset="0" fontFamily="none" fontWeight="none" fontSize="none" textAnchor="none" style={{"mixBlendMode": "normal"}}>
            <path d="M0,172v-172h172v172z" fill="none" />
            <g className="notification_logo" fill="#c7ced1">
              <path d="M86,14.33333c-5.934,0 -10.75,4.816 -10.75,10.75v4.98307c-18.53885,4.77861 -32.25,21.56799 -32.25,41.60026v43l-11.00195,8.28646h-0.014c-2.06676,1.31602 -3.31796,3.59669 -3.31738,6.04688c0,3.95804 3.20863,7.16667 7.16667,7.16667h50.16667h50.16667c3.95804,0 7.16667,-3.20863 7.16667,-7.16667c0.00058,-2.45018 -1.25062,-4.73086 -3.31739,-6.04687l-11.01595,-8.28646v-43c0,-20.03227 -13.71115,-36.82165 -32.25,-41.60026v-4.98307c0,-5.934 -4.816,-10.75 -10.75,-10.75zM71.66667,143.33333c0,7.88333 6.45,14.33333 14.33333,14.33333c7.88333,0 14.33333,-6.45 14.33333,-14.33333z" />
            </g>
          </g>
        </svg>
        <div className="user_profile">
          <img src={user ? user.url : ''} alt=""/>
          <span>{user ? user.user : ''}</span>
        </div>
        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
             width="20" height="20"
             viewBox="0 0 172 172"
             style={{"fill": "#000000"}}
             className="log_out_button"
             onClick={handleLogOut}
        >
          <g fill="none" fillRule="nonzero" stroke="none" strokeWidth="1" strokeLinecap="butt" strokeLinejoin="miter" strokeMiterlimit="10" strokeDasharray="" strokeDashoffset="0" fontFamily="none" fontWeight="none" fontSize="none" textAnchor="none" style={{"mixBlendMode": "normal"}}>
            <path d="M0,172v-172h172v172z" fill="none" />
            <g fill="#cccccc">
              <path d="M86.1075,6.7725c-43.61533,0 -79.12,35.50467 -79.12,79.12c0,43.61533 35.50467,79.12 79.12,79.12c23.3781,0 44.43061,-10.21499 58.89656,-26.3711c1.72575,-1.81717 2.32705,-4.42892 1.56965,-6.81778c-0.7574,-2.38887 -2.7537,-4.17703 -5.21122,-4.66789c-2.45752,-0.49085 -4.98757,0.39324 -6.60453,2.30786c-11.97388,13.37286 -29.26937,21.7889 -48.65047,21.7889c-36.17891,0 -65.36,-29.18109 -65.36,-65.36c0,-36.17891 29.18109,-65.36 65.36,-65.36c19.37733,0 36.67279,8.41529 48.65047,21.7889c1.61696,1.91462 4.14701,2.7987 6.60452,2.30785c2.45751,-0.49086 4.45382,-2.27902 5.21121,-4.66788c0.7574,-2.38886 0.1561,-5.0006 -1.56964,-6.81778c-14.46904,-16.15534 -35.52158,-26.37109 -58.89656,-26.37109zM133.9786,54.86531c-2.79841,0.00347 -5.31595,1.7014 -6.36771,4.29465c-1.05175,2.59324 -0.42817,5.56514 1.57724,7.51691l12.33562,12.33563h-62.51125c-2.48118,-0.03509 -4.78904,1.2685 -6.03987,3.41161c-1.25083,2.1431 -1.25083,4.79369 0,6.93679c1.25083,2.1431 3.55869,3.4467 6.03987,3.41161h62.51125l-12.33562,12.33563c-1.79734,1.72562 -2.52135,4.28808 -1.89282,6.69912c0.62853,2.41104 2.5114,4.29391 4.92245,4.92245c2.41104,0.62853 4.9735,-0.09548 6.69912,-1.89282l23.4686,-23.46859c1.71744,-1.30466 2.72377,-3.33912 2.71849,-5.49591c-0.00528,-2.15678 -1.02155,-4.1863 -2.74536,-5.48253l-23.44172,-23.44172c-1.29693,-1.33318 -3.07834,-2.08452 -4.93828,-2.08281z" />
            </g>
          </g>
        </svg>
      </div>
    </div>
  );
}

const mapStateToProps = rootState => ({
  profile: rootState.profile
})

const mapDispatchToProps = {
  setAuthUserAction
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);