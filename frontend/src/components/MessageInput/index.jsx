import React, { useState } from 'react';
import { connect } from 'react-redux';

import './MessageInput.css';

const MessageInput = ({ profile, sendMessage }) => {
  const [text, setText] = useState('');

  const submit = () => {
    if (text === '') {
      return;
    }
    const { user } = profile;
    sendMessage({ text, userId: user.id, avatar: user.url, user: user.user });
    setText('');
  };

  const handleKeyDown = event => {
    if (event.key === 'ArrowUp' && text === '') {
      // const lastMessage = getLastUserMessage();
      // if (!lastMessage) {
      //   return;
      // }
      // toggleExpandedMessage(lastMessage);
    }
    if (event.key !== 'Enter') {
      return;
    }
    submit();
  };

  return (
    <div className="message_input">
      <input placeholder="Type something..."
             onChange={ev => setText(ev.target.value)}
             value={text}
             onKeyDown={handleKeyDown}
      />
      <button onClick={submit}>Send</button>
    </div>
  );
};

const mapStateToProps = rootState => ({
  profile: rootState.profile
});

export default connect(
  mapStateToProps
)(MessageInput);