import React from 'react';

import './DaySeparator.css';

const DaySeparator = ({ date }) => {
  return (
    <div className="day_separator">
      {date}
    </div>
  );
};

export default DaySeparator;