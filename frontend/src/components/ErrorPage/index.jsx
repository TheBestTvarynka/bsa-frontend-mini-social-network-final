import React from 'react';

import './ErrorPage.css';
import { Link } from 'react-router-dom';

const ErrorPage = () => {
  return (
    <div className="error_page">
      <span className="title_error">Error</span>
      <span className="error_description">Page you are looking does not exist or you have not access to it.</span>
      <Link to="/">
        <button className="go_back">Go back</button>
      </Link>
    </div>
  );
};

export default ErrorPage;