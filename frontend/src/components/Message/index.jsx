import React from 'react';
import { formatDate } from '../../containers/Messenger/actions';
import { Link } from 'react-router-dom';

import './Message.css';

const Message = ({ data, user, deleteMessage, reactMessage }) => {
  const { id, text, avatar, createdAt, editedAt, userId, isLiked, user: username } = data;

  return (
    <div className={(userId === user.id) ? 'message main' : 'message'}>
      {(userId !== user.id) &&
        (avatar ? <img src={avatar}  alt=""/> : <span style={{'minWidth': '40px'}} />)
      }
      <span className={(userId === user.id) ? 'message_text main_text' : 'message_text'}>
        <span className="username">{username}</span>
        {(userId === user.id) &&
          <span className="message_actions">
            <Link to={`/message/${id}`} style={{ textDecoration: 'none'}}>
              <span>Edit</span>
            </Link>
            <span onClick={() => deleteMessage(id)}>Delete</span>
          </span>
        }
        <span>{text}</span>
        <span className="date">{(editedAt ? 'edited ' : '') + formatDate(createdAt)}</span>
      </span>
      {(userId !== user.id) &&
        (isLiked
          ? <img src="https://img.icons8.com/fluent/48/000000/filled-like.png"
                 alt=""
                 className="like"
                 onClick={() => reactMessage(id)}
          />
          : <img src="https://img.icons8.com/material-outlined/64/000000/filled-like.png"
                 alt=""
                 className="like"
                 onClick={() => reactMessage(id)}
          />)
      }
    </div>
  );
};

export default Message;