import React, { useState } from 'react';
import { connect } from 'react-redux';
import { authUserAction } from '../../containers/Messenger/actions';
import { Redirect } from 'react-router-dom';

import './LoginPage.css';

const LoginPage = ({ profile, authUserAction }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = () => {
    authUserAction({
      username,
      password
    });
  };

  return (
    <div className="login_page">
      <div className="login_header">
        <img src="https://img.icons8.com/cotton/64/000000/chat.png" alt="" className="login_chat_logo"/>
        <span className="login_app_name">AmazingChatApp</span>
      </div>
      {profile
      ? <Redirect to={{ pathname: '/', state: { from: '/login' } }} />
      : (
        <form onSubmit={event => {
          event.preventDefault();
          handleLogin();
        }} className="login_form">
          <label className="login_title">Hmmm, I see you haven't logged in to your account so far :( </label>
          <label className="input_title">Username</label>
          <input type="text" className="login_input" placeholder="e. g. capmap" onChange={event => setUsername(event.target.value)}/>
          <label className="input_title">Password</label>
          <input type="password" className="login_input" onChange={event => setPassword(event.target.value)}/>
          <input type="button" className="login_button" onClick={handleLogin} value="Continue" />
          <button className="register_button">Need an account? Lets create one...</button>
        </form>
        )}
    </div>
  );
};

const mapStateToProps = rootState => ({
  profile: rootState.profile
});

const mapDispatchToProps = {
  authUserAction
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);