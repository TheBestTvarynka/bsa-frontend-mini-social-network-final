import React  from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { deleteUserAction } from '../../containers/Messenger/actions';

import './UserListItem.css';

const UserListItem = ({ user, deleteUserAction }) => {
  const {id, url, user: username } = user;

  const handleDeleteUser = () => {
    deleteUserAction(id);
  };

  return (
    <div className="user_item">
      <div className="user_info">
        <img src={url} alt=""/>
        <span>{username}</span>
      </div>
      <div className="users_actions">
        <Link to={`/user/${id}`} style={{ textDecoration: 'none' }}>
          <span className="edit">Edit</span>
        </Link>
        <span className="delete" onClick={handleDeleteUser}>Delete</span>
      </div>
    </div>
  );
};

const mapDispatchToProps = {
  deleteUserAction
}

export default connect(
  null,
  mapDispatchToProps
)(UserListItem);