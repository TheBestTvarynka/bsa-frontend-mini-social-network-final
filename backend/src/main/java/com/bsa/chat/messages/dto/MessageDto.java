package com.bsa.chat.messages.dto;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.UUID;

public class MessageDto {
    public UUID id;
    public String text;
    public UUID userId;
    public String user;
    public String avatar;
    public String createdAt;
    public String editedAt;
	public Boolean isLiked;

    public MessageDto(UUID id, String text, UUID userId, String user, String avatar, String createdAt, String editedAt, Boolean isLiked) {
        this.id = id;
        this.text = text;
        this.userId = userId;
        this.user = user;
        this.avatar = avatar;
        this.createdAt = createdAt;
        this.editedAt = editedAt;
		this.isLiked = isLiked;
    }

    public static int compareTo(MessageDto m1, MessageDto m2) {
        OffsetDateTime d1 = OffsetDateTime.parse(m1.createdAt, DateTimeFormatter.ISO_DATE_TIME);
        OffsetDateTime d2 = OffsetDateTime.parse(m2.createdAt, DateTimeFormatter.ISO_DATE_TIME);
        return d1.compareTo(d2);
    }

}
