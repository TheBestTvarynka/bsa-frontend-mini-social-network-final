package com.bsa.chat.messages;

import com.bsa.chat.messages.dto.MessageDto;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class MessageService {

    private Map<UUID, MessageDto> messages = null;

    public List<MessageDto> generateMessages() {
        messages = new HashMap<>();
        messages.put(UUID.fromString("80f08600-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80f08600-1b8f-11e8-9629-c7eca82aa7bd"), "I don’t *** understand. It is the Panama accounts", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-13T19:48:12.936Z", "", false));
        messages.put(UUID.fromString("80e00b40-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80e00b40-1b8f-11e8-9629-c7eca82aa7bd"), "Tells exactly what happened.", UUID.fromString("533b5230-1b8f-11e8-9629-c7eca82aa7bd"), "Wendy", "https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng", "2020-07-16T19:48:42.481Z", "2020-07-13T19:48:47.481Z", false));
        messages.put(UUID.fromString("80e03259-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80e03259-1b8f-11e8-9629-c7eca82aa7bd"), "You were doing your daily bank transfers and…", UUID.fromString("533b5230-1b8f-11e8-9629-c7eca82aa7bd"), "Wendy", "https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng", "2020-07-14T19:48:56.273Z", "", false));
        messages.put(UUID.fromString("80e03258-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80e03258-1b8f-11e8-9629-c7eca82aa7bd"), "Yes, like I’ve been doing every *** day without red *** flag", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-14T19:49:14.480Z", "", false));
        messages.put(UUID.fromString("80e03257-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80e03257-1b8f-11e8-9629-c7eca82aa7bd"), "There is never been a *** problem.", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-14T19:48:28.769Z", "", false));
        messages.put(UUID.fromString("80e03256-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80e03256-1b8f-11e8-9629-c7eca82aa7bd"), "Why this account?", UUID.fromString("4b003c20-1b8f-11e8-9629-c7eca82aa7bd"), "Helen", "https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ", "2020-07-14T19:49:33.195Z", "", false));
        messages.put(UUID.fromString("80e03255-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80e03255-1b8f-11e8-9629-c7eca82aa7bd"), "I do not *** know! I do not know!", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-15T19:49:45.821Z", "", false));
        messages.put(UUID.fromString("80e03254-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80e03254-1b8f-11e8-9629-c7eca82aa7bd"), "What the ** is a red flag anyway?", UUID.fromString("5328dba1-1b8f-11e8-9629-c7eca82aa7bd"), "Ben", "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg", "2020-07-15T19:50:07.708Z", "", false));
        messages.put(UUID.fromString("80e03253-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80e03253-1b8f-11e8-9629-c7eca82aa7bd"), "You said you could handle things", UUID.fromString("4b003c20-1b8f-11e8-9629-c7eca82aa7bd"), "Helen", "https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ", "2020-07-15T19:53:02.483Z", "", true));
        messages.put(UUID.fromString("80e03252-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80e03252-1b8f-11e8-9629-c7eca82aa7bd"), "I did what he taught me.", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-16T19:53:17.272Z", "2020-07-15T19:53:50.272Z", false));
        messages.put(UUID.fromString("80e03251-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80e03251-1b8f-11e8-9629-c7eca82aa7bd"), "it is not my fucking fault!", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-16T19:53:49.171Z", "", true));
        messages.put(UUID.fromString("80e03250-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80e03250-1b8f-11e8-9629-c7eca82aa7bd"), "Can you fix this? Can you fix it?", UUID.fromString("533b5230-1b8f-11e8-9629-c7eca82aa7bd"), "Wendy", "https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng", "2020-07-16T19:56:51.491Z", "", false));
        messages.put(UUID.fromString("80e03249-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80e03249-1b8f-11e8-9629-c7eca82aa7bd"), "Her best is gonna get us all killed.", UUID.fromString("4b003c20-1b8f-11e8-9629-c7eca82aa7bd"), "Helen", "https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ", "2020-07-16T19:57:07.965Z", "2020-07-16T19:57:15.965Z", true));
        messages.put(UUID.fromString("80e03248-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80e03248-1b8f-11e8-9629-c7eca82aa7bd"), "I do not know how!", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-16T19:58:06.686Z", "", true));
        messages.put(UUID.fromString("80e03247-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80e03247-1b8f-11e8-9629-c7eca82aa7bd"), "it means that the accounts frozen that cause the feds might think that there is a crime being committed.", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-16T19:52:04.375Z", "", false));
        messages.put(UUID.fromString("80e03246-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80e03246-1b8f-11e8-9629-c7eca82aa7bd"), "Like by me", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-16T19:52:15.334Z", "", true));
        messages.put(UUID.fromString("80e03245-1b8f-11e8-9629-c7eca82aa7bd"), new MessageDto(UUID.fromString("80e03245-1b8f-11e8-9629-c7eca82aa7bd"), "aaaha!", UUID.fromString("5328dba1-1b8f-11e8-9629-c7eca82aa7bd"), "Ben", "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg", "2020-07-16T19:58:17.878Z", "", false));
        return new ArrayList<>(messages.values());
    }

    public List<MessageDto> getAllMessages() {
        return (messages == null ? generateMessages() : new ArrayList<>(messages.values())).stream()
                .sorted(MessageDto::compareTo)
                .collect(Collectors.toList());
    }

    public Optional<MessageDto> getMessageById(UUID id) {
        if (messages == null) {
            generateMessages();
        }
        if (!messages.containsKey(id)) {
            return Optional.empty();
        }
        return Optional.of(messages.get(id));
    }

    public MessageDto updateMessage(MessageDto newMessage) {
        if (newMessage.id == null) {
            newMessage.id = UUID.randomUUID();
        }
        messages.put(newMessage.id, newMessage);
        return newMessage;
    }

	public void deleteById(UUID id) {
		if (messages == null) {
			return;
		}
		messages.remove(id);
	}

}
