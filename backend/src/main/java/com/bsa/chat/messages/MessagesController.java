package com.bsa.chat.messages;

import com.bsa.chat.messages.dto.MessageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/messages")
public class MessagesController {

    @Autowired
    private MessageService messageService;

    @GetMapping
    public List<MessageDto> getAllMessages() {
        return messageService.getAllMessages();
    }

    @GetMapping("/{id}")
    public Optional<MessageDto> getMessageById(@PathVariable UUID id) {
        return messageService.getMessageById(id);
    }

    @PostMapping
    public MessageDto updateMessage(@RequestBody MessageDto newMessage) {
        return messageService.updateMessage(newMessage);
    }

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteMessage(@PathVariable UUID id) {
		messageService.deleteById(id);
	}

}
