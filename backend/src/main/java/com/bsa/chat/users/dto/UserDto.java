package com.bsa.chat.users.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class UserDto {
    public UUID id;
    public String user;
    public String url;
}
