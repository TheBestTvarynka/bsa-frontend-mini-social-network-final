package com.bsa.chat.users;

import com.bsa.chat.users.dto.UserDto;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UsersService {
    Map<UUID, UserDto> users;
    Map<String, String> passwords;
    Map<String, UserRoles> roles;

    public String getPasswordByUsername(String username) {
        if (passwords == null) {
            generateUsers();
        }
        return passwords.get(username);
    }

    public UserDto findUserByUsername(String username) {
        if (users == null) {
            generateUsers();
        }
        return users.values().stream()
                .filter(user -> user.getUser().equals(username))
                .findFirst()
                .orElseThrow();
    }

    public String getRoleByUsername(String username) {
        if (roles == null) {
            generateUsers();
        }
        return roles.get(username).toString();
    }

    public List<UserDto> generateUsers() {
        passwords = new HashMap<>();
        passwords.put("admin", "admin");
        passwords.put("Asan", "apple@cat");
        passwords.put("Helen", "cyrly~gr");

        roles = new HashMap<>();
        roles.put("admin", UserRoles.ADMIN);
        roles.put("Asan", UserRoles.USER);
        roles.put("Helen", UserRoles.USER);

        users = new HashMap<>();
        users.put(UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), new UserDto(UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA"));
        users.put(UUID.fromString("533b5230-1b8f-11e8-9629-c7eca82aa7bd"), new UserDto(UUID.fromString("533b5230-1b8f-11e8-9629-c7eca82aa7bd"), "Wendy", "https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng"));
        users.put(UUID.fromString("4b003c20-1b8f-11e8-9629-c7eca82aa7bd"), new UserDto(UUID.fromString("4b003c20-1b8f-11e8-9629-c7eca82aa7bd"), "Helen", "https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ"));
        users.put(UUID.fromString("533b5230-1b9f-11e8-9629-c7eca82aa7bd"), new UserDto(UUID.fromString("533b5230-1b9f-11e8-9629-c7eca82aa7bd"), "Pasha", "https://i.imgur.com/dKDpaU5.jpg"));
        users.put(UUID.fromString("534b5230-1b8f-11e8-9629-c7eca82aa7bd"), new UserDto(UUID.fromString("534b5230-1b8f-11e8-9629-c7eca82aa7bd"), "Misha", "https://i.imgur.com/hG4Th4U.jpg"));
        users.put(UUID.fromString("533b5230-1b8f-11a8-9629-c7eca82aa7bd"), new UserDto(UUID.fromString("533b5230-1b8f-11a8-9629-c7eca82aa7bd"), "Asan", "https://i.imgur.com/Y8DGAuj.jpg"));
        users.put(UUID.fromString("533b5210-1b8f-11e8-9629-c7eca82aa7bd"), new UserDto(UUID.fromString("533b5210-1b8f-11e8-9629-c7eca82aa7bd"), "Yter", "https://i.imgur.com/hIjmHms.jpg"));
        users.put(UUID.fromString("3befd790-d44f-4ec3-bea6-2e388f7ee28e"), new UserDto(UUID.fromString("3befd790-d44f-4ec3-bea6-2e388f7ee28e"), "admin", "https://i.imgur.com/Au030og.png"));
        return new ArrayList<>(users.values());
    }

    public List<UserDto> getAllUsers() {
        return users == null ? generateUsers() : new ArrayList<>(users.values());
    }

    public Optional<UserDto> getUserById(UUID id) {
        if (users == null) {
            generateUsers();
        }
        if (!users.containsKey(id)) {
            return Optional.empty();
        }
        return Optional.of(users.get(id));
    }

    public UserDto updateUser(UserDto newUser) {
        if (newUser.id == null) {
            newUser.id = UUID.randomUUID();
        }
        users.put(newUser.id, newUser);
        return newUser;
    }

	public void deleteUserById(UUID id) {
        if (users == null) {
            return;
        }
        users.remove(id);
	}

}
