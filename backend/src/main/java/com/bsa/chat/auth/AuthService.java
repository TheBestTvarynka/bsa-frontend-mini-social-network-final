package com.bsa.chat.auth;

import com.bsa.chat.auth.dto.AuthUserDTO;
import com.bsa.chat.auth.dto.UserLoginDTO;
import com.bsa.chat.users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class AuthService {

    @Autowired
    private UsersService userService;

    public AuthUserDTO login(UserLoginDTO user) throws Exception {
        String password = userService.getPasswordByUsername(user.getUsername());
        if (password == null || !password.equals(user.getPassword())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "wrong username or password");
        }
        return new AuthUserDTO("a2671bb5c4fe227adb2195b2db8790295d143a1dc24aa2b1a34e4200e3f6b4f7",
                userService.getRoleByUsername(user.getUsername()),
                userService.findUserByUsername(user.getUsername()));
    }

}
