package com.bsa.chat.auth.dto;

import com.bsa.chat.users.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AuthUserDTO {
    private String token;
    private String role;
    private UserDto user;
}